import React, { PureComponent } from 'react';
import { View, Text, Dimensions, StyleSheet } from 'react-native';
function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text>
        No internet connections
      </Text>
    </View>
  );
}
class OfflineNotice extends PureComponent {
  render() {
    return <MiniOfflineSign />;
  }
}
const styles = StyleSheet.create({
  offlineContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
export default OfflineNotice;