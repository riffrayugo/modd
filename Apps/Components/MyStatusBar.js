import React, { Component } from 'react';
import { StatusBar, View, Platform, Dimensions } from 'react-native';

const deviceHeight = Dimensions.get('window').height
const deviceWidth = Dimensions.get('window').width
let STATUSBAR_HEIGHT = StatusBar.currentHeight;
let platform = Platform.OS
const isIphone = platform === 'ios'
const isIphoneX = platform === 'ios' && deviceHeight === 812 && deviceWidth === 375
if (isIphone) {
  STATUSBAR_HEIGHT = isIphoneX ? 42 : 20
}
class MyStatusBar extends Component {
  render () {
    const {backgroundColor, barStyle} = this.props;
    return (
      <View style={{ backgroundColor, height: STATUSBAR_HEIGHT }}>
        <StatusBar translucent backgroundColor={backgroundColor} barStyle={barStyle} />
      </View>
    )
  }
}
export default MyStatusBar;