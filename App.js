/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import OfflineNotice from './Apps/Components/OfflineNotice'
import MyStatusBar from './Apps/Components/MyStatusBar'
import {
  BackHandler,
  Button,
  NetInfo,
  Text,
  View,
  StyleSheet,
  Animated,
  TouchableOpacity,
  Platform,
  Dimensions,
  ActivityIndicator,
  // WebView
} from 'react-native';
import { WebView } from 'react-native-webview';
let marginTop = 0;
const deviceHeight = Dimensions.get('window').height
const deviceWidth = Dimensions.get('window').width
const platform = Platform.OS
const isIphone = platform === 'ios'
const isIphoneX = platform === 'ios' && deviceHeight === 812 && deviceWidth === 375
if (isIphone) {
  marginTop = isIphoneX ? 42 : 20
}

export default class App extends Component {
  // webviewRefs = {};
  constructor(properties) {
    super(properties);
    this.springValue = new Animated.Value(100);
    this.state = {
      loading: false,
      isConnected: true,
      refreshing: false,
      enablePTR: false,
      url: 'http://dukcapildharmasrayakabmobilonline.id',
      iosNavigation: false,
      clickCount: 0
    };
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }
  // handler to check connectivity
  handleConnectivityChange = isConnected => {
    if (isConnected) {
      this.setState({ isConnected });
    } else {
      this.setState({ isConnected });
    }
  };
  _spring() {
    this.webviewRefs && this.webviewRefs.goBack();
    this.setState({ clickCount: 1 }, () => {
      Animated.sequence([
        Animated.spring(
          this.springValue,
          {
            toValue: -.01 * deviceHeight,
            friction: 5,
            duration: 300,
            useNativeDriver: true,
          }
        ),
        Animated.timing(
          this.springValue,
          {
            toValue: 100,
            duration: 300,
            useNativeDriver: true,
          }
        ),

      ]).start(() => {
        this.setState({ clickCount: 0 });
      });
    });
  }
  handleBackPress = () => {
    this.state.clickCount == 1 ? BackHandler.exitApp() : this._spring();
    return true;
  }
  handleBackIOS = () => {
    this.webviewRefs && this.webviewRefs.goBack();
    this.setState({ iosNavigation: false })
  }
  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.webviewRefs && this.webviewRefs.reload();
  }
  onMessage(e) {
    this.setState({ enablePTR: e.nativeEvent.data == 0 })
  }
  onNavigationStateChange(navState) {
    // handle loading 
    navState.loading ? this.setState({ loading: true }) : this.setState({ loading: false })

    // if (navState.url.match('google')) {
    //   if (!navState.loading) {
    //     var wb_url = navState.url;
    //     var lastPart = wb_url.substr(-3);
    //     if (lastPart === "pdf"  || lastPart === "docx" || lastPart === "doc" && navState.url.match('google') === null) {
    //       const DEFAULT_URL = 'https://docs.google.com/viewerng?embedded=true&url=' + wb_url;
    //       this.setState({ url: DEFAULT_URL })
    //     }
    //     !navState.loading?this.setState({ iosNavigation:true }) : this.setState( {iosNavigation: false} )
    //   }
    // } else {
    //   var wb_url = navState.url;
    //   var lastPart = wb_url.substr(-3);
    //   if (lastPart === "pdf" || lastPart === "docx" || lastPart === "doc") {
    //     const DEFAULT_URL = 'https://docs.google.com/viewerng?embedded=true&url=' + wb_url;
    //     this.setState({ url: DEFAULT_URL })
    //   }
    // }
  }

  render() {
    if (!this.state.isConnected) {
      return (
        <OfflineNotice />
      );
    } else {
      return (
        <View style={styles.container}>
          <MyStatusBar
            barStyle="light-content"
            backgroundColor="#D5112C"
          />
          {
            <WebView
              ref={ref => (this.webviewRefs = ref)}
              useWebKit={true}
              allowUniversalAccessFromFileURLs={true}
              // domStorageEnabled={true}
              // geolocationEnabled={true}
              javaScriptEnable={true}
              // onNavigationStateChange={this.onNavigationStateChange.bind(this)}
              // onLoadEnd={() => { this.setState({ refreshing: false }) }}
              // onError={() => { this.setState({ refreshing: false }) }}
              source={{ uri: this.state.url }}
            />
          }
          <Animated.View style={[styles.animatedView, { transform: [{ translateY: this.springValue }] }]}>
            <Text style={styles.exitTitleText}>Tekan back lagi untuk keluar</Text>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => BackHandler.exitApp()}
            >
              <Text style={styles.exitText}>Exit</Text>
            </TouchableOpacity>
          </Animated.View>
        </View >
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    marginTop: 0
  },
  animatedView: {
    width: deviceWidth,
    backgroundColor: "#000000",
    elevation: 2,
    position: "absolute",
    bottom: 0,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  exitTitleText: {
    textAlign: "center",
    color: "#ffffff",
    marginRight: 10,
  },
  exitText: {
    color: "#e5933a",
    paddingHorizontal: 10,
    paddingVertical: 3
  }
});
